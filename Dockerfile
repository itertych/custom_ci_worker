# Start from the base SLC6 or CC7 slave images
FROM gitlab-registry.cern.ch/ci-tools/ci-worker:cc7
# Use tag 'slc6' instead of 'cc7' for SLC6
# The FROM statement can be overriden in the GitLab-CI build

ENV INSTALL_DIR=/srv/worker

# install custom packages (in this example, ghostscript and ghostscript-devel)
### TODO: set the list of packages as necessary
RUN yum install -y sudo gcc python-devel libsqlite3x-devel mysql-devel postgresql-devel && yum clean all

# Adding tox
RUN pip install tox
ENV PYTHONPATH=$PYTHONPATH:/usr/lib/python2.6/site-packages
ENV PATH=$PATH:/usr/bin

# Prepare user and work directory
RUN useradd -u 1001 pa && \
    mkdir -p ${INSTALL_DIR} && \
    chown pa:pa ${INSTALL_DIR} && \
    mkdir -p /opt/app-root/jenkins && \
    chmod -R 777 /opt/app-root/jenkins
    
WORKDIR ${INSTALL_DIR}
